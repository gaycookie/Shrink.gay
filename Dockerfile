FROM node:19.8

WORKDIR /var/shrink
COPY . .

RUN npm install

EXPOSE 8080
CMD [ "npm", "start" ]