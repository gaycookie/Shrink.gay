import { Shrink } from ".";
import { ShareXSchema } from "./schemas";

export class ShareX {
  public static GenerateSXCU(app: Shrink, token: string): ShareXSchema {
    return {
      Version: '15.0.0', Name: 'ShrinkGay',
      DestinationType: 'URLShortener',
      RequestMethod: 'POST', Body: 'FormURLEncoded',
      RequestURL: `${app.config.externalUrl}/api/url`,
      Arguments: { url: '{input}', token: token },
      URL: `${app.config.externalUrl}/{json:_hash}`,
      ErrorMessage: '{json:error}'
    };
  }
}