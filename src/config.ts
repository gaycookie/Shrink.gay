export class Config {
  public externalUrl = process.env.EXTERNAL_URL || 'http://localhost:8080';
  public mongo = process.env.MONGO_URI || 'mongodb://database:27017/shrink';
}