import { Router } from "express";
import crypto from "crypto";
import { Shrink } from "..";
import { UrlSchema } from "../schemas";

export default (app: Shrink) => {
  const router = Router();

  router.get('/url/:query', async (req, res) => {
    app.database.findOneUrl(req.params.query).then(result => {
      if (!result) return res.status(404).json({ error: 'URL was not found' });
      return res.json(result);
    });
  });

  router.post('/url', async (req, res) => {
    if (!req.body) return res.status(400).json({ error: 'No body was provided.' });
    if (!req.body.token) return res.status(400).json({ error: 'No token was provided.' });
    if (!req.body.url) return res.status(400).json({ error: 'No url was provided.' });

    const user = await app.database.findOneUserByToken(req.body.token);
    if (!user) return res.status(401).json({ error: 'Not authorized.' });

    const url: UrlSchema = {
      _hash: crypto.randomBytes(4).toString('hex'),
      _original: req.body.url, 
      _user: user._id, 
      _creation: new Date()
    };

    app.database.insertUrl(url).then(result => {
      if (!result) return res.status(500).json({ error: 'Something went wrong on our side.' });
      return res.json(result);
    });
  });
  
  return router;
}

