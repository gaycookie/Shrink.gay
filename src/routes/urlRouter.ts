import { Router } from "express";
import { Shrink } from "../.";

export default (app: Shrink) => {
  const router = Router();

  router.get('/:query', async (req, res) => {
    const result = await app.database.findOneUrl(req.params.query);
    if (!result) return res.status(404).render('errors/404');

    app.database.addVisitById(result._id);
    res.redirect(302, result._original);
  });

  return router;
}