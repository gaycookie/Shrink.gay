import { ObjectId } from "mongoose";

export interface UrlSchema {
  _id?: ObjectId;
  _hash: string;
  _custom?: string;
  _original: string;
  _creation: Date;
  _user: ObjectId;
  _expires?: Date
}

export interface UserSchema {
  _id?: ObjectId;
  _name: string;
  _email: string;
  _password: string;
  _active: boolean;
  _admin: boolean;
  _created: Date;
  _token: string;
}

export interface VisitSchema {
  _id?: ObjectId;
  _url: ObjectId;
  _timestamp: Date;
}

export interface ShareXSchema {
  Version: string,
  Name: string,
  DestinationType: string,
  RequestMethod: string,
  RequestURL: string,
  Body: string,
  Arguments: {
    url: string,
    token: string
  },
  URL: string,
  ErrorMessage: string
}