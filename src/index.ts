import express from "express";
import favicon from "serve-favicon";
import path from "path";
import { Config } from "./config";
import { Database } from "./database";

import urlRouter from "./routes/urlRouter";
import apiRouter from "./routes/apiRouter";

export class Shrink {
  readonly config = new Config();
  readonly database = new Database(this);
  private server = express();
  private port = 8080;

  constructor() {
    this.initializeMiddleware();
    this.initializeRouter();

    this.server.listen(this.port, () => console.log(`Web server has been started successfully!`));
  }

  private initializeMiddleware() {
    this.server.set('view engine', 'ejs');
    this.server.use(express.json());
    this.server.use(express.urlencoded({ extended: true }));
    this.server.use(favicon(path.join(__dirname, '..', 'public', 'favicon.ico')));
  }

  private initializeRouter() {
    this.server.use('/api', apiRouter(this));
    this.server.use('/', urlRouter(this));

    this.server.get("*", (req, res) => res.render('errors/404'));
  }
}

new Shrink();