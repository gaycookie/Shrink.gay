import crypto from "crypto";
import mongoose, { Connection, ObjectId } from "mongoose";
import { Shrink } from "./index";
import { ShareXSchema, UrlSchema, UserSchema, VisitSchema } from "./schemas";
import { ShareX } from "./sharex";

export class Database {
  private database: Connection;

  constructor(readonly app: Shrink) {
    mongoose.connect(this.app.config.mongo);

    this.database = mongoose.connection;
    this.database.on('error', () => console.log('Error connecting to the database'));
    this.database.once('open', async () => {
      console.log('Successfully connected to the database!')

      const count = await this.database.collection<UserSchema>('users').countDocuments()
      if (count) return;
      
      const newUser = {
        _name: 'admin', _email: 'example@example.com',
        _password: crypto.createHash('md5').update('admin').digest('hex'),
        _active: true, _admin: true, _created: new Date(), _token: crypto.randomUUID()
      };

      await this.database.collection<UserSchema>('users').insertOne(newUser);
      console.log('Created new admin user!');
      console.log(ShareX.GenerateSXCU(app, newUser._token));
    });
  }

  public async findOneUrlById(id: ObjectId) {
    const result = await this.database.collection<UrlSchema>('urls').findOne({ _id: id });
    return result;
  }

  public async findOneUrl(query: string) {
    const result = await this.database.collection<UrlSchema>('urls').findOne({ $or: [{ _hash: query }, { _custom: query }] });
    return result;
  }

  public async findOneUserByToken(token: string) {
    const result = await this.database.collection<UserSchema>('users').findOne({ _token: token });
    return result;
  }

  public async insertUrl(url: UrlSchema) {
    const result = await this.database.collection<UrlSchema>('urls').insertOne(url);
    if (!result.acknowledged) return false;

    const insertedUrl = await this.findOneUrlById(result.insertedId);
    return insertedUrl;
  }

  public async addVisitById(id: ObjectId) {
    await this.database.collection<VisitSchema>('visits').insertOne({ _url: id, _timestamp: new Date() });
  }
}